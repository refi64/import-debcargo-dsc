#!/bin/bash

set -eo pipefail

if [[ "$#" -ne 2 && "$#" -ne 3 ]]; then
  echo 'usage: import-debcargo-dsc.sh <distribution> <dsc> [<component>]' >&2
  exit 1
fi

distro="$1"
dsc="$(realpath $2)"
[[ -n "$3" ]] && component="$3"

package=$(awk '/Source:/{print $2}' "$dsc")

gbp import-dsc \
  --author-is-committer --author-date-is-committer-date \
  --upstream-branch="upstream/$distro" \
  --debian-branch="debian/unreleased/$distro" \
  --debian-tag="debian/unreleased/%(version)s" \
  --no-sign-tags --no-pristine-tar \
  "$dsc"

[[ -d .git ]] || cd "$package"

if [[ -e ".git/refs/heads/apertis/$distro" ]]; then
  apertis-pkg-merge-updates \
    --upstream "debian/unreleased/$distro" \
    --downstream "apertis/$distro"
  # Undo the script's authorship changes.
  git config --remove-section user
else
  git checkout -b "apertis/$distro"
fi

pristine-lfs import-dsc "$dsc"

version=$(dpkg-parsechangelog -S Version)
sed -i \
  -e 's/UNRELEASED-FIXME-AUTOGENERATED-DEBCARGO/UNRELEASED/' \
  -e "s/($version)/($version+apertis0)/" \
  debian/changelog

export DEBFULLNAME=$(git config user.name)
export DEBEMAIL=$(git config user.email)

dch 'Import unreleased package.' -D "$distro"

if [[ -n "$component" ]]; then
  mkdir -p debian/apertis
  echo "$component" > debian/apertis/component

  dch -a "Set component to $component."
  git add debian/apertis/component
fi

git add debian/changelog
git commit -m "Import unreleased version $version+apertis0."
